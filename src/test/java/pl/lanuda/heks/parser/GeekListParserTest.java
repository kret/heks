package pl.lanuda.heks.parser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.test.context.junit4.SpringRunner;
import pl.lanuda.heks.parser.model.GeekListXML;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeekListParserTest {

    private static final LocalDateTime LAST_EDIT_DATE = LocalDateTime.of(2015, 9, 15, 2, 54, 26);

    @Autowired
    private XStreamMarshaller marshaller;

    @Test
    public void parseGeekList() throws IOException {
        InputStream inputStream = GeekListParserTest.class.getResourceAsStream("/geekList.xml");
        GeekListXML geekListXML = (GeekListXML) marshaller.unmarshalInputStream(inputStream);
        assertNotNull("geek list xml", geekListXML);
        assertNotNull("id", geekListXML.getId());
        assertEquals("id", 174654L, geekListXML.getId().longValue());
        assertEquals("last edit date", LAST_EDIT_DATE, geekListXML.getLastEditDate());
        assertEquals("username", "W Eric Martin", geekListXML.getOwnersUserName());
        assertEquals("title", "Spiel 2015 Preview", geekListXML.getTitle());
        assertEquals("geek list items", 8, geekListXML.getItems().size());

        GeekListXML.GeekListItemXML geekListItemXML = geekListXML.getItems().get(7);
        assertEquals("list item id", 3986471L, geekListItemXML.getGeekListItemId().longValue());
        assertEquals("list item subtype", "boardgame", geekListItemXML.getSubType());
        assertEquals("list item object id", 170225L, geekListItemXML.getObjectId().longValue());
        assertEquals("list item object name", "DRCongo", geekListItemXML.getObjectName());
        assertEquals("list item username", "W Eric Martin", geekListItemXML.getOwnerUserName());
        assertEquals("list item thumbs", 35L, geekListItemXML.getThumbsCount().longValue());
        assertEquals("list item image id", 2345066L, geekListItemXML.getImageId().longValue());
        assertEquals("list item body", "• Price €40", geekListItemXML.getBody());
    }
}

package pl.lanuda.heks.parser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.test.context.junit4.SpringRunner;
import pl.lanuda.heks.parser.model.GameXML;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameParserTest {

    @Autowired
    private XStreamMarshaller marshaller;

    @Test
    public void parseBoardGameExpansion() throws IOException {
        InputStream inputStream = GameParserTest.class.getResourceAsStream("/boardgameexpansion.xml");
        GameXML gameXML = (GameXML) marshaller.unmarshalInputStream(inputStream);
        assertNotNull("gameXML", gameXML);
        assertNotNull("gameXML.items", gameXML.getItems());
        assertEquals("gameXML.items.size", 1, gameXML.getItems().size());

        GameXML.Item item = gameXML.getItems().get(0);
        assertNotNull("gameXML.items[0]", item);
        assertEquals("gameXML.items[0].id", 181971L, item.getId().longValue());
        assertEquals("gameXML.items[0].type", "boardgameexpansion", item.getType());
        assertEquals("gameXML.items[0].thumbnail", "//cf.geekdo-images.com/images/pic2617819_t.jpg", item.getThumbnail());
        assertEquals("gameXML.items[0].description", "The expansion Power Grid: The Stock Companies offers players a completely new game experience,\n            specifically three new ways to play either Power Grid or Power Grid Deluxe.&#10;&#10;The main game\n            is The Stock Companies: As shareholders, the players want to get control over different stock companies so\n            they can acquire the greatest wealth.&#10;&#10;The two variants are a little bit closer to the\n            well-known Power Grid experience.&#10;&#10;&bull; The Biggest Electricity Distributors: In this\n            first variant the players are shareholders who try to get shares of the most successful companies: the\n            companies that supply electricity to the most cities.&#10;&#10;&bull; The Competition of the\n            Private Companies: In this second variant the players control their private stock companies and try to\n            establish them successfully on the electricity market. They sell shares of their own companies to generate\n            more cash and they are able to buy shares of other players&rsquo; companies. Thus, they can also earn a\n            portion of the other players&rsquo; successes.&#10;&#10;If you own all Power Grid maps and the\n            expansion New Power Plants, this expansion offers 222 new possibilities to play Power Grid and Power Grid\n            deluxe.&#10;&#10;The rules for this expansion are based on module 9 &quot;Shares&quot; of\n            the game 504. As 504 is a modular game system, we are able to take that module and mix it with Power Grid.\n            The results are three different options to play this expansion. For the main game, we focused on the first\n            option of the shares module, so you will discover a lot of surprises. Both variants are a little bit closer\n            to the well-known Power Grid experience; they are for gamers who want to experiment with the game system.&#10;&#10;\n        ", item.getDescription());
        assertNotNull("gameXML.items[0].minPlayers", item.getMinPlayers());
        assertNotNull("gameXML.items[0].minPlayers.value", item.getMinPlayers().getValue());
        assertEquals("gameXML.items[0].minPlayers.value", (short) 2, item.getMinPlayers().getValue().shortValue());
        assertNotNull("gameXML.items[0].maxPlayers", item.getMaxPlayers());
        assertNotNull("gameXML.items[0].maxPlayers.value", item.getMaxPlayers().getValue());
        assertEquals("gameXML.items[0].maxPlayers.value", (short) 6, item.getMaxPlayers().getValue().shortValue());
        assertNotNull("gameXML.items[0].minPlayingTime", item.getMinPlayingTime());
        assertNotNull("gameXML.items[0].minPlayingTime.value", item.getMinPlayingTime().getValue());
        assertEquals("gameXML.items[0].minPlayingTime.value", (short) 120, item.getMinPlayingTime().getValue().shortValue());
        assertNotNull("gameXML.items[0].maxPlayingTime", item.getMaxPlayingTime());
        assertNotNull("gameXML.items[0].maxPlayingTime.value", item.getMaxPlayingTime().getValue());
        assertEquals("gameXML.items[0].maxPlayingTime.value", (short) 120, item.getMaxPlayingTime().getValue().shortValue());
        assertNotNull("gameXML.items[0].playingTime", item.getPlayingTime());
        assertNotNull("gameXML.items[0].playingTime.value", item.getPlayingTime().getValue());
        assertEquals("gameXML.items[0].playingTime.value", (short) 120, item.getPlayingTime().getValue().shortValue());
        assertNotNull("gameXML.items[0].minAge", item.getMinAge());
        assertNotNull("gameXML.items[0].minAge.value", item.getMinAge().getValue());
        assertEquals("gameXML.items[0].minAge.value", (short) 13, item.getMinAge().getValue().shortValue());

        List<GameXML.Name> names = item.getNames();
        assertNotNull("gameXML.items[0].names", names);
        assertEquals("gameXML.items[0].names.size", 7, names.size());
        GameXML.Name primaryName = names.stream()
                .filter(name -> "primary".equals(name.getType()))
                .findAny()
                .orElse(null);
        assertNotNull("gameXML.items[0].names[type='primary']", primaryName);
        assertEquals("gameXML.items[0].names[type='primary'].value", "Power Grid: The Stock Companies", primaryName.getValue());
        assertNotNull("gameXML.items[0].names[type='primary'].sortIndex", primaryName.getSortIndex());
        assertEquals("gameXML.items[0].names[type='primary'].sortIndex", (short) 1, primaryName.getSortIndex().shortValue());

        GameXML.Item.FieldWithShortValue yearPublished = item.getYearPublished();
        assertNotNull("gameXML.items[0].yearPublished", yearPublished);
        assertNotNull("gameXML.items[0].yearPublished.value", yearPublished.getValue());
        assertEquals("gameXML.items[0].yearPublished.value", (short) 2015, yearPublished.getValue().shortValue());

        List<GameXML.Poll> polls = item.getPolls();
        assertNotNull("gameXML.items[0].polls", polls);
        assertEquals("gameXML.items[0].polls", 3, polls.size());
        GameXML.Poll languageDependencePoll = polls.stream()
                .filter(poll -> "language_dependence".equals(poll.getName()))
                .findAny()
                .orElse(null);
        assertNotNull("gameXML.items[0].polls[name='language_dependence']", languageDependencePoll);
        assertEquals("gameXML.items[0].polls[name='language_dependence'].name", "language_dependence", languageDependencePoll.getName());
        assertEquals("gameXML.items[0].polls[name='language_dependence'].title", "Language Dependence", languageDependencePoll.getTitle());
        assertNotNull("gameXML.items[0].polls[name='language_dependence'].totalVotes", languageDependencePoll.getTotalVotes());
        assertEquals("gameXML.items[0].polls[name='language_dependence'].totalVotes", 2L, languageDependencePoll.getTotalVotes().longValue());

        List<GameXML.Poll.Results> resultsList = languageDependencePoll.getResults();
        assertNotNull("gameXML.items[0].polls[name='language_dependence'].results", resultsList);
        assertEquals("gameXML.items[0].polls[name='language_dependence'].results", 1, resultsList.size());

        GameXML.Poll.Results results = resultsList.get(0);
        assertNotNull("gameXML.items[0].polls[name='language_dependence'].results[0]", results);
        assertNotNull("gameXML.items[0].polls[name='language_dependence'].results[0].results", results.getResults());
        assertEquals("gameXML.items[0].polls[name='language_dependence'].results[0].results", 5, results.getResults().size());

        GameXML.Versions versions = item.getVersions();
        assertNotNull("gameXML.items[0].versions", versions);
        List<GameXML.Item> versionItems = item.getVersions().getItems();
        assertNotNull("gameXML.items[0].versions.items", versionItems);
        assertEquals("gameXML.items[0].versions.items", 7, versionItems.size());
        GameXML.Item version = versionItems.get(0);
        assertNotNull("gameXML.items[0].versions.items[0]", version);
        assertNotNull("gameXML.items[0].versions.items[0].yearPublished", version.getYearPublished());
        assertNotNull("gameXML.items[0].versions.items[0].yearPublished.value", version.getYearPublished());
        assertEquals("gameXML.items[0].versions.items[0].yearPublished.value", (short) 2015, version.getYearPublished().getValue().shortValue());
        assertNotNull("gameXML.items[0].versions.items[0].links", version.getLinks());
        assertEquals("gameXML.items[0].versions.items[0].links", 5, version.getLinks().size());
        List<GameXML.Item.Link> versionPublishers = version.getLinks().stream()
                .filter(link -> "boardgamepublisher".equals(link.getType()))
                .collect(Collectors.toList());
        assertEquals("gameXML.items[0].versions.items[0].links[type='boardgamepublisher']", 1, versionPublishers.size());
        GameXML.Item.Link versionPublisher = versionPublishers.get(0);
        assertEquals("gameXML.items[0].versions.items[0].links[type='boardgamepublisher'][0].value", "Rio Grande Games", versionPublisher.getValue());
        List<GameXML.Item.Link> versionLanguages = version.getLinks().stream()
                .filter(link -> "language".equals(link.getType()))
                .collect(Collectors.toList());
        assertEquals("gameXML.items[0].versions.items[0].links[type='language']", 1, versionLanguages.size());
        GameXML.Item.Link versionLanguage = versionLanguages.get(0);
        assertEquals("gameXML.items[0].versions.items[0].links[type='language'][0].value", "English", versionLanguage.getValue());
    }
}

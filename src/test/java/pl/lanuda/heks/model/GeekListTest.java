package pl.lanuda.heks.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.lanuda.heks.repository.GeekListRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GeekListTest {

    @Autowired
    private GeekListRepository geekListRepository;

    @Test
    public void testSaveAndFindOfValidGeekList() {
        LocalDateTime now = LocalDateTime.now();
        GeekList geekList = new GeekList(1L, now, "Llama", "Kret");
        List<GeekListItem> geekListItems = new ArrayList<>();
        geekListItems.add(createGeekListItemFor(1L, geekList));
        geekListItems.add(createGeekListItemFor(2L, geekList));
        geekList.setGeekListItems(geekListItems);
        geekListRepository.saveAndFlush(geekList);

        List<GeekList> geekListsFromDB = geekListRepository.findAll();
        assertNotNull("geek lists from DB", geekListsFromDB);
        assertEquals("geek lists from DB", 1, geekListsFromDB.size());

        GeekList geekListFromDB = geekListsFromDB.get(0);
        assertNotNull("geek list from DB - id", geekListFromDB.getId());
        assertEquals("geek list from DB - geek list id", 1L, geekListFromDB.getId().longValue());
        assertEquals("geek list from DB - last edit date", now, geekListFromDB.getLastEditDate());
        assertEquals("geek list from DB - owner's user name", "Llama", geekListFromDB.getOwnersUserName());
        assertEquals("geek list from DB - title", "Kret", geekListFromDB.getTitle());

        List<GeekListItem> geekListItemsFromDB = geekListFromDB.getGeekListItems();
        assertEquals("geek list items", 2, geekListItemsFromDB.size());
        assertGeekListItem(1, geekListItemsFromDB.get(0));
        assertGeekListItem(2, geekListItemsFromDB.get(1));
    }

    private GeekListItem createGeekListItemFor(long no, GeekList geekList) {
        GeekListItem geekListItem = new GeekListItem();
        geekListItem.setId(no);
        geekListItem.setGame(new Game(no, "geek list item " + no));
        geekListItem.setGeekList(geekList);

        return geekListItem;
    }

    private void assertGeekListItem(long no, GeekListItem geekListItem) {
        assertNotNull("geek list item " + no, geekListItem.getId());
        assertEquals("geek list item " + no + " - item id", no, geekListItem.getId().longValue());
        assertEquals("geek list item " + no + " - item name", "geek list item " + no, geekListItem.getGame().getName());
    }
}

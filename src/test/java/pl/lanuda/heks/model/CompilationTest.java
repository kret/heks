package pl.lanuda.heks.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.lanuda.heks.repository.CompilationRepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CompilationTest {

    @Autowired
    private CompilationRepository compilationRepository;

    @Test
    public void testSaveAndFindOfValidCompilation() {
        Compilation compilation = new Compilation("Essen 2016", true, LocalDateTime.now());
        compilationRepository.saveAndFlush(compilation);

        getAllCompilationsFromDBAndAssertEqualsTo(compilation);
    }

    @Test
    public void testDeleteSingleCompilation() {
        LocalDateTime now = LocalDateTime.now();

        Compilation c1 = new Compilation("Essen 2015", true, now);
        Compilation c2 = new Compilation("Essen 2016", true, now);
        compilationRepository.save(Arrays.asList(c1, c2));
        compilationRepository.flush();

        compilationRepository.delete(c1.getId());

        getAllCompilationsFromDBAndAssertEqualsTo(c2);
    }

    @Test
    public void testSaveAndFindWithList() {
        LocalDateTime now = LocalDateTime.now();

        GeekList g1 = new GeekList(1L, now, "Llama 1", "Kret 1");
        GeekList g2 = new GeekList(2L, now, "Llama 2", "Kret 2");

        Compilation compilation = new Compilation("Essen 2016", true, now);
        compilation.setGeekLists(Arrays.asList(g1, g2));
        compilationRepository.saveAndFlush(compilation);

        Compilation compilationFromDB = getAllCompilationsFromDBAndAssertEqualsTo(compilation);
        assertEquals("compilation geeklists", 2, compilationFromDB.getGeekLists().size());
    }

    private Compilation getAllCompilationsFromDBAndAssertEqualsTo(Compilation compilation) {
        List<Compilation> compilationsFromDB = compilationRepository.findAll();
        assertNotNull("compilations from DB", compilationsFromDB);
        assertEquals("compilations from DB", 1, compilationsFromDB.size());

        Compilation compilationFromDB = compilationsFromDB.get(0);
        assertNotNull("compilation from DB - id", compilationFromDB.getId());
        assertEquals("compilation from DB - name", compilation.getName(), compilationFromDB.getName());
        assertEquals("compilation from DB - active", compilation.getActive(), compilationFromDB.getActive());
        assertEquals("compilation from DB - due date", compilation.getDueDate(), compilationFromDB.getDueDate());

        return compilationFromDB;
    }
}

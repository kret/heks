package pl.lanuda.heks.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.lanuda.heks.repository.UserRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testUserRegistration() {
        User user = new User();
        user.setUsername("kret");
        user.setPassword("password");
        user.setEnabled(true);
        userRepository.save(user);

        User userFromDB = userRepository.findOne("kret");
        assertNotNull("user", userFromDB);
        assertEquals("user.username", "kret", userFromDB.getUsername());
        assertEquals("user.password", "password", userFromDB.getPassword());
        assertEquals("user.enabled", true, userFromDB.isEnabled());
    }
}

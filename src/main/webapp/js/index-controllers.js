var heks = angular.module('heks', [ 'ngResource', 'ngMasonry', 'ngMaterial' ]);

heks

.factory('CompilationsFactory', function($http, Compilation) {
	
	this.compilations = Compilation.query();
	
	return {
		allCompilations: this.compilations
	}
})

.factory('Compilation', ['$resource', function($resource){

	return $resource('/api/compilation', {}, {
	    addGeekList: {
	        method: 'POST',
	        url: '/api/compilation/:compilationId/list/:id'
	    },
	    deleteGeekList: {
	        method: 'DELETE',
	        url: '/api/compilation/:compilationId/list/:id'
	    },
	    getDetails: {
	        method: 'GET',
	        url: '/api/compilation/:compilationId/game'
	    }
	});

}])

.factory('getParameterByName', function() {
    return function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
})

.controller('MyCompilations', function($scope, Compilation, CompilationsFactory) {
		

	$scope.$watch(function(){ return CompilationsFactory.allCompilations }, function(newValue, oldValue) {
		$scope.geekListCompilations = CompilationsFactory.allCompilations;
	});
	
    $scope.createNewCompilation = function(newCompilation) {
        Compilation.save(newCompilation);
    };

    $scope.addListToCompilation = function(_compilation, _newGeekList) {
        Compilation.addGeekList({compilationId: _compilation.id}, _newGeekList);
    }

    $scope.removeListFromCompilation = function(_compilation, _geekList) {
        Compilation.deleteGeekList({compilationId: _compilation.id, id: _geekList.id});
    }
})

.controller('Compilation', function($scope, $resource, Compilation, getParameterByName) {

    $scope.compilation = Compilation.getDetails({compilationId: getParameterByName('id')});

	$scope.filter_config = {
		players : {
			min : 1,
			max : 20,
			lower : 5,
			upper : 10
		},
		time : {
			min : 1,
			max : 20,
			lower : 3,
			upper : 12
		}
	};


	$scope.filters = {
		minPlayers: 4,
		maxPlayers: 8
	}

    var language_codes = {
        Catalan: "ca",
        Chinese: "zh",
        Czech: "cs",
        Danish: "da",
        Dutch: "nl",
        English: "en",
        Esperanto: "eo",
        Finnish: "fi",
        French: "fr",
        German: "de",
        Greek: "el",
        Hungarian: "hu",
        Icelandic: "is",
        Italian: "it",
        Japanese: "ja",
        Norwegian: "no",
        Polish: "pl",
        Portuguese: "pt",
        Russian: "ru",
        Slovak: "sk",
        Spanish: "es",
        Swedish: "sv"
    };

    var gameRatingResource = $resource('/api/game/:id/rating');

	$scope.rating = {
		languageDependence: [1,2,3,4,5]
	}

	$scope.rating_values = {
	    'NOT_RATED': "Not Rated",
        'IGNORE': "Ignore",
        'LIKE': "Like",
        'WANT': "Want",
        'NEED': "Need"
	};

	$scope.preferences = {
		min_players: {
			min: 1,
			max: 2
		}
	}

    $scope.get_lang_code = function(lang) {
        return language_codes[lang] || lang;
    }

    $scope.is_lang_known = function(langs) {
        return langs.indexOf("Polish") >= 0 || langs.indexOf("English") >= 0;
    }

    $scope.save_rating = function() {
        gameRatingResource.save({id: this.game.id}, this.game.myRating);
    }

    $scope.new_rating = function(_value) {
        this.game.myRating.value = _value;
        this.save_rating();
    }

    $scope.min_players = function(_value) {
    	return _value < this.preferences.min_players.min || _value > this.preferences.min_players.max;
    }

    /*
	$scope.compilation = {
		name: "Essen preview for 2016",
		dueDate:"12/09/2016",

		games: [{
			people: [" Vangelis Bagiartakis","Grzegorz Bobrowski", "Gong Studios", "Tomasz Jedruszek", "Naomi Robinson "],
			name: "Fields of Green ",
			description: "As with the board game Camel Up, Camel Up Cards has players betting on camels as they make their way down a racetrack. Each player has some knowledge about which camels can move — and how far they can go when they do finally decide to move — but they can't be sure of when each one will move, so they'll just have to guess which ones will end in front of the others, hoping to earn a bit of money while doing so.",
			type:'boardgame',
			languageDependence: 2,
			thumbnail: " http://placehold.it/75x75",
			minPlayers:1,
			maxPlayers: 4,
			playingTime: 234,
			minAge: 12,
			myRating: {value:"LIKE", comment:"Ddd"},
			versions: [
				{
					year: 2016,
					publishers: ["FunBox Jogos"],
					languages: ["Portuguese"]
				},
				{
					year: 2014,
					publishers: ["Swan Panasia Co., Ltd."],
					languages: ["Chinese"]
				},
				{
					year: 2014,
					publishers: ["Zvezda"],
					languages: ["Russian"]
				},
				{
					year: 2014,
					publishers: ["HomoLudicus"],
					languages: ["Spanish"]
				},
				{
					year: 2013,
					publishers: ["White Goblin Games"],
					languages: ["Dutch"]
				},
				{
					year: 2013,
					publishers: ["Filosofia Éditions", "Z-Man Games"],
					languages: ["English", "French"]
				},
				{
					year: 2013,
					publishers: ["Feuerland Spiele"],
					languages: ["German"]
				},
				{
					year: 2013,
					publishers: ["Cranio Creations"],
					languages: ["Italian"]
				},
				{
					year: 2013,
					publishers: ["テンデイズゲームズ  - Ten Days Games"],
					languages: ["Japanese"]
				},
				{
					year: 2013,
					publishers: ["Bard Centrum Gier", "MINDOK"],
					languages: ["Czech", "Polish"]
				}
			],
			friendRatings: [{
				username: "Kret",
				value:"WANT",
				comment: "Lorem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit ametpsum dolor sit amet."
			}]
		},{
			name: "2. My Essen game to be evaluated if its title is not to  its title is not to longif its  is not to longif its title is not to longif  title is not to long if its title is not to longif its title is not to longif its title is not to long or to long to disp",
			description: "Fields of Green takes place in the second half of the 20th century. Players take the role of farm owners trying to expand their property and business. By adding fields, livestock and facilities, they build an economic engine that will bring them closer to victory. Fields of Green, inspired by Among the Stars, is played over four rounds (years) during which players draft cards and add them to their ever-expanding farms. At the end of each year comes the harvest season when they must water their fields, feed their livestock, and pay maintenance costs in order to receive valuable resources that will allow them to further expand in the next year. Through various means, player eventually convert their wealth to victory points, and the player who gathers the most by the end of the fourth year wins.",
			thumbnail: " http://placehold.it/75x75",
			languageDependence: 5,
			minPlayers:4,
			type:'xxx',
			maxPlayers: 4,
			playingTime: 234,
			minAge: 12,
			friendRatings: [{
				username: "kret",
				value: "IGNORE",
				comment: "Dolor sit amet."
				
			}]
		},{
			name: "3. My Essen game to be evaluated if its title is not to long",
			description: "Lorem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit ametpsum dolor sit amet, consectetur adipiscing elit. Etiam bibendum arcu nec fermentum posuere.",
			type:'boardgame',
			thumbnail: " http://placehold.it/75x75",
			languageDependence: 1,
			gameData: {
			minPlayers:1,
			maxPlayers: 4,
			playingTime: 234,
			minAge: 12
			},
			friendRatings: [{
				username: "kret",
				value: "NEED"
				
			}]
		},{
			name: "4. My Essen game to be evaluated if its title is not to long",
			description: "ss",
			type:'boardgame',
			minPlayers:1,
			maxPlayers: 4,
			playingTime: 234,
			minAge: 12,
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		},{
			name: "5. My Essen game to be evaluated if its title is not to long",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.",
			type:'boardgame',
			minPlayers:1,
			maxPlayers: 4,
			playingTime: 234,
			minAge: 12,
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		},
		{
			name: "1. Game not to long",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere. Duis euismod variustincidunt. Quisque porttitor luctus commodo. Donec vel mauris elit.Sed quis eleifend sem. Lorem ipsum dolor sit amet, consecteturadipiscing elit. Proin sit amet velit sed nibh molestie aliquet egetut eros. Donec eget tortor maximus, pulvinar dolor sit amet, eleifendelit. In odio leo, accumsan a arcu hendrerit, posuere dapibus nisl. ",
			type:'boardgame',
			minPlayers:1,
			maxPlayers: 4,
			playingTime: 234,
			minAge: 12,
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		},{
			name: "2. My Essen game to be evaluated if its title is not to long",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.",
			type:'boardgame',
			gameData: {
				minPlayers:1,
				maxPlayers: 4,
				playingTime: 234,
				minAge: 12
			},
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		},{
			name: "3. My Essen game to be evaluated if its title is not to long",
			description: "Lorem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit ametrem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit ametpsum dolor sit amet, consectetur adipiscing elit. Etiam bibendum arcu nec fermentum posuere.",
			type:'boardgame',
			gameData: {
				minPlayers:1,
				maxPlayers: 4,
				playingTime: 234,
				minAge: 12
			},
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		},{
			name: "4. My Essen game to be evaluated if its title is not to long",
			description: "ss",
			type:'boardgame',
			gameData: {
				minPlayers:1,
				maxPlayers: 4,
				playingTime: 234,
				minAge: 12
			},
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		},{
			name: "5. My Essen game to be evaluated if its title is not to long",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam bibendum arcu nec fermentum posuere.",
			type:'boardgame',
			gameData: {
				minPlayers:1,
				maxPlayers: 4,
				playingTime: 234,
				minAge: 12
			},
			friendRatings: [{
				username: "Kret",
				value:"LIKE",
				comment: "dssdfdsfsdf"
			}]
		}]}
		*/
})

.filter('cut', function () {
        return function (value, max) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
              //Also remove . and , so its gives a cleaner result.
              if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                lastspace = lastspace - 1;
              }
              value = value.substr(0, lastspace);
            }
        

            return value + ' ...';
        };
})

.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('default')
	    .primaryPalette('light-green')
	    .accentPalette('amber')
	    .warnPalette('lime');
});

;
(function(module) {
	"use strict";
	// source: https://gist.github.com/kuitos/89e8aa538f0a507bd682
	module.controller('RangeSliderController', ['$scope', function($scope) {
		$scope.$watchGroup(['min', 'max'], minMaxWatcher);
		$scope.$watch('lowerValue', lowerValueWatcher);
		$scope.$watch('upperValue', upperValueWatcher);

		function minMaxWatcher() {
			$scope.lowerMax = $scope.upperValue;
			$scope.upperMin = $scope.lowerValue;

			if (!$scope.lowerValue || $scope.lowerValue < $scope.min) {
				$scope.lowerValue = $scope.min;
			}
			if (!$scope.upperValue || $scope.upperValue > $scope.max) {
				$scope.upperValue = $scope.max;
			}
			updateUpperWidth();
			updateLowerWidth();
		}

		function lowerValueWatcher() {
			if ($scope.lowerValue > $scope.upperValue) {
				$scope.lowerValue = $scope.upperValue;
				return;
			}
			$scope.upperMin = $scope.lowerValue;

			updateUpperWidth();
		}

		function upperValueWatcher() {
			if ($scope.upperValue < $scope.lowerValue) {
				$scope.upperValue = $scope.lowerValue;
				return;
			}

			$scope.lowerMax = $scope.upperValue;

			updateLowerWidth();
		}

		function updateUpperWidth() {
			$scope.upperWidth = (($scope.max - $scope.lowerValue) / ($scope.max - $scope.min) * 100) + "%";
			// console.log("A", $scope.max - $scope.lowerValue, "------", $scope.min, $scope.lowerValue, $scope.upperValue, $scope.max)
				// if($scope.lowerValue > ($scope.upperValue - 1) && $scope.upperValue < $scope.max) {
				// 	$scope.upperValue = $scope.lowerValue + 1;
				// }
		}

		function updateLowerWidth() {

			$scope.lowerWidth = (( ($scope.upperValue - $scope.min) / ($scope.max - $scope.min)) * 100) + "%";
			// console.log("B", $scope.min + $scope.upperValue, "------", $scope.min, $scope.lowerValue, $scope.upperValue, $scope.max)
				// $scope.lowerWidth = ((($scope.max-($scope.lowerValue ))/($scope.max-$scope.min)) * 100) + "%";
				// if($scope.upperValue > ($scope.lowerValue + 1) && $scope.lowerValue < $scope.min) {
				// 	$scope.lowerValue = $scope.upperValue - 1;
				// }
		}
	}]);

	module.directive('rangeSlider', function() {
		return {
			scope: {
				max: '=',
				min: '=',
				minGap: '=',
				step: '=',
				lowerValue: "=lowerValue",
				upperValue: "=upperValue"
			},
			template: [
				'<div class="range-slider-container">',
				'<div class="range-slider-left" ng-style="{width: lowerWidth}">',
				'<md-slider md-discrete aria-label="lowerValue" step="{{step}}" ng-model="lowerValue" min="{{min}}" max="{{lowerMax}}"></md-slider>',
				'</div>',
				'<div class="range-slider-right" ng-style="{width: upperWidth}">',
				'<md-slider md-discrete aria-label="upperValue" step="{{step}}" ng-model="upperValue" min="{{upperMin}}" max="{{max}}"></md-slider>',
				'</div>',
				'</div>'
			].join(''),
			controller: 'RangeSliderController'
		};
	});

}(angular.module("mdRangeSlider", ['ngMaterial'])));
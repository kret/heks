var heks = angular.module('heks', [ 'ngResource', 'ngMaterial', 'mdRangeSlider', 'infinite-scroll']);

heks

.filter('filterByRating', function() {
    return function(items, only) {
        return items.filter(function (item) {
        	return only[item.myRating.value];
        })
    };
})

.factory('CompilationsFactory', function($http, Compilation) {
	
	this.compilations = Compilation.query();
	
	return {
		allCompilations: this.compilations
	}
})

.factory('Compilation', ['$resource', function($resource){

	return $resource('/api/compilation/:compilationId', {}, {
	    addGeekList: {
	        method: 'POST',
	        url: '/api/compilation/:compilationId/list/:id'
	    },
	    deleteGeekList: {
	        method: 'DELETE',
	        url: '/api/compilation/:compilationId/list/:id'
	    },
	    getGames: {
	        method: 'GET',
	        url: '/api/compilation/:compilationId/games',
	        isArray: true
	    }
	});

}])

.factory('getParameterByName', function() {
    return function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
})

.controller('MyCompilations', function($scope, Compilation, CompilationsFactory) {
		

	$scope.$watch(function(){ return CompilationsFactory.allCompilations }, function(newValue, oldValue) {
		$scope.geekListCompilations = CompilationsFactory.allCompilations;
	});
	
    $scope.createNewCompilation = function(newCompilation) {
        Compilation.save(newCompilation);
    };

    $scope.addListToCompilation = function(_compilation, _newGeekList) {
        Compilation.addGeekList({compilationId: _compilation.id}, _newGeekList);
    }

    $scope.removeListFromCompilation = function(_compilation, _geekList) {
        Compilation.deleteGeekList({compilationId: _compilation.id, id: _geekList.id});
    }
})

.controller('Compilation', function($scope, $resource, $sce, Compilation, getParameterByName) {

    var page = 0;

    $scope.compilation = Compilation.get({compilationId: getParameterByName('id')});
    $scope.games = [];

    $scope.load_more_games = function() {
        if ($scope.page_is_loading) {
            return;
        }

        $scope.page_is_loading = true;

        Compilation.getGames({compilationId: getParameterByName('id'), page: page, sort: 'name'}, function(items) {
            // in order to sort by other props:
            // sort: ['name', 'id'] === sort: 'name,id'
            // sort: ['name,desc', 'id']     <= can specify multiple directions
            page += 1;
            for (var i = 0, l = items.length; i < l; i += 1) {
                var game = items[i];
                game.description = $sce.trustAsHtml(game.description);
                $scope.games.push(game);
            }

            $scope.page_is_loading = false;
        });
    };

    $scope.page_is_loading = false;

	$scope.filter_config = {
		players : {
			min : 1,
			max : 20,
			lower : 5,
			upper : 10
		},
		time : {
			min : 1,
			max : 20,
			lower : 3,
			upper : 12
		},
		ratings: {
			'NOT_RATED': true,
			'IGNORE': false,
			'LIKE': true,
			'WANT': true,
			'NEED': true
		}
	};


	$scope.filters = {
		minPlayers: 4,
		maxPlayers: 8
	}

    var language_codes = {
        Catalan: "ca",
        Chinese: "zh",
        Czech: "cs",
        Danish: "da",
        Dutch: "nl",
        English: "en",
        Esperanto: "eo",
        Finnish: "fi",
        French: "fr",
        German: "de",
        Greek: "el",
        Hungarian: "hu",
        Icelandic: "is",
        Italian: "it",
        Japanese: "ja",
        Norwegian: "no",
        Polish: "pl",
        Portuguese: "pt",
        Russian: "ru",
        Slovak: "sk",
        Spanish: "es",
        Swedish: "sv"
    };

    var gameRatingResource = $resource('/api/game/:id/rating');

	$scope.rating = {
		languageDependence: [1,2,3,4,5]
	}

	$scope.rating_values = {
	    'NOT_RATED': "Not Rated",
        'IGNORE': "Ignore",
        'LIKE': "Like",
        'WANT': "Want",
        'NEED': "Need"
	};

	$scope.preferences = {
		min_players: {
			min: 1,
			max: 2
		}
	}

	$scope.filter_single_rating = function(rating_value) {
		$scope.filter_config.ratings[rating_value] = !$scope.filter_config.ratings[rating_value];
	}



    $scope.get_lang_code = function(lang) {
        return language_codes[lang] || lang;
    }

    $scope.is_lang_known = function(langs) {
        return langs.indexOf("Polish") >= 0 || langs.indexOf("English") >= 0;
    }

    $scope.save_rating = function() {
        var _game = this.game;
        _game.hasError = false;
        gameRatingResource.save({id: _game.id}, _game.myRating, function (saved_rating, responseHeaders, status, statusText) {
            _game.myRating.comment = saved_rating.comment;
            _game.myRating.value = saved_rating.value;
            _game.myRating.version = saved_rating.version;
        }, function (httpResponse) {
            _game.hasError = true;
        });
    }

    $scope.new_rating = function(_value) {
        this.game.myRating.value = _value;
        this.save_rating();
    }

    $scope.min_players = function(_value) {
    	return _value < this.preferences.min_players.min || _value > this.preferences.min_players.max;
    }

})

.filter('cut', function () {
        return function (value, max) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
              //Also remove . and , so its gives a cleaner result.
              if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                lastspace = lastspace - 1;
              }
              value = value.substr(0, lastspace);
            }
        

            return value + ' ...';
        };
})

.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('default')
	    .primaryPalette('light-green')
	    .accentPalette('amber')
	    .warnPalette('lime');
});

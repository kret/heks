package pl.lanuda.heks.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeekListRouteBuilder extends RouteBuilder {

    private final DataFormat dataFormat;

    @Autowired
    public GeekListRouteBuilder(DataFormat dataFormat) {
        this.dataFormat = dataFormat;
    }

    @Override
    public void configure() throws Exception {

        from("file:esb/geek-list-import?move=.done&moveFailed=.failed&antInclude=*.xml&readLock=fileLock")
            .unmarshal(dataFormat)
            .to("log:pl.lanuda.heks")
            .to("bean:geekListService?method=refreshGeekListFromXML");

        from("file:esb/thing-import?move=.done&moveFailed=.failed&antInclude=*.xml&readLock=fileLock")
            .unmarshal(dataFormat)
            .to("log:pl.lanuda.heks")
            .to("bean:gameService?method=refreshGameFromXML");

        from("timer://gamesTimer?period=5s")
            .to("bean:gameService?method=findGamesForRefresh");

        from("timer://geekListTimer?period=5s")
            .to("bean:geekListService?method=findGeekListsForRefresh");

        from("timer://bggTimer?period=10s")
            .to("bean:boardGameGeekDownloaderService?method=download");
    }
}

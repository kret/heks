package pl.lanuda.heks.dto;

import pl.lanuda.heks.model.RatingValue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class GameDTO {

    private Long id;
    private String name;
    private String type;
    private String thumbnail;
    private Short yearPublished;
    private Short minPlayers;
    private Short maxPlayers;
    private Short playingTime;
    private Short minPlayingTime;
    private Short maxPlayingTime;
    private Short minAge;
    private Double languageDependenceRatio;
    private String description;
    private Map<Long, Set<Long>> itemsOnGeekLists = new HashMap<>();
    private Set<GameVersionDTO> versions = new HashSet<>();
    private Set<RatingDTO> friendRatings = new HashSet<>();
    private RatingDTO myRating = new RatingDTO();
    private Long version;

    public GameDTO() {
    }

    public GameDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Short getYearPublished() {
        return yearPublished;
    }

    public void setYearPublished(Short yearPublished) {
        this.yearPublished = yearPublished;
    }

    public Short getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Short minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Short getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Short maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Short getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(Short playingTime) {
        this.playingTime = playingTime;
    }

    public Short getMinPlayingTime() {
        return minPlayingTime;
    }

    public void setMinPlayingTime(Short minPlayingTime) {
        this.minPlayingTime = minPlayingTime;
    }

    public Short getMaxPlayingTime() {
        return maxPlayingTime;
    }

    public void setMaxPlayingTime(Short maxPlayingTime) {
        this.maxPlayingTime = maxPlayingTime;
    }

    public Short getMinAge() {
        return minAge;
    }

    public void setMinAge(Short minAge) {
        this.minAge = minAge;
    }

    public Double getLanguageDependenceRatio() {
        return languageDependenceRatio;
    }

    public void setLanguageDependenceRatio(Double languageDependenceRatio) {
        this.languageDependenceRatio = languageDependenceRatio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description != null && description.length() >= 500) {
            description = description.substring(0, 500).concat(" ...");
        }
        this.description = description;
    }

    public Map<Long, Set<Long>> getItemsOnGeekLists() {
        return itemsOnGeekLists;
    }

    public void setItemsOnGeekLists(Map<Long, Set<Long>> itemsOnGeekLists) {
        this.itemsOnGeekLists = itemsOnGeekLists;
    }

    public void addGeekListItem(Long geekListId, Long geekListItemId) {
        if (!itemsOnGeekLists.containsKey(geekListId)) {
            itemsOnGeekLists.put(geekListId, new HashSet<>());
        }
        itemsOnGeekLists.get(geekListId).add(geekListItemId);
    }

    public Set<GameVersionDTO> getVersions() {
        return versions;
    }

    public void setVersions(Set<GameVersionDTO> versions) {
        this.versions = versions;
    }

    public void addGameVersion(Short yearPublished, Set<String> publishers, Set<String> languages, Long version) {
        versions.add(new GameVersionDTO(yearPublished, publishers, languages, version));
    }

    public Set<RatingDTO> getFriendRatings() {
        return friendRatings;
    }

    public void setFriendRatings(Set<RatingDTO> friendRatings) {
        this.friendRatings = friendRatings;
    }

    public RatingDTO getMyRating() {
        return myRating;
    }

    public void setMyRating(RatingDTO myRating) {
        this.myRating = myRating;
    }

    public void addGameRating(String username, RatingValue value, String comment, boolean ownRating, Long version) {
        RatingDTO rating = new RatingDTO(username, value, comment, version);
        if (ownRating) {
            myRating = rating;
        } else {
            friendRatings.add(rating);
        }
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameDTO)) return false;
        GameDTO gameDTO = (GameDTO) o;
        return Objects.equals(id, gameDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    private class GameVersionDTO {

        private Short yearPublished;
        private Set<String> publishers;
        private Set<String> languages;
        private Long version;

        GameVersionDTO(Short yearPublished, Set<String> publishers, Set<String> languages, Long version) {
            this.yearPublished = yearPublished;
            this.publishers = publishers;
            this.languages = languages;
            this.version = version;
        }

        public Short getYearPublished() {
            return yearPublished;
        }

        public void setYearPublished(Short yearPublished) {
            this.yearPublished = yearPublished;
        }

        public Set<String> getPublishers() {
            return publishers;
        }

        public void setPublishers(Set<String> publishers) {
            this.publishers = publishers;
        }

        public Set<String> getLanguages() {
            return languages;
        }

        public void setLanguages(Set<String> languages) {
            this.languages = languages;
        }

        public Long getVersion() {
            return version;
        }

        public void setVersion(Long version) {
            this.version = version;
        }
    }
}

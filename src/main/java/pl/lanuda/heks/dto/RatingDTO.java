package pl.lanuda.heks.dto;

import pl.lanuda.heks.model.RatingValue;

public class RatingDTO {

    private String username;
    private RatingValue value = RatingValue.NOT_RATED;
    private String comment;
    private Long version;

    public RatingDTO() {
    }

    public RatingDTO(String username, RatingValue value, String comment, Long version) {
        this.username = username;
        this.value = value;
        this.comment = comment;
        this.version = version;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public RatingValue getValue() {
        return value;
    }

    public void setValue(RatingValue value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}

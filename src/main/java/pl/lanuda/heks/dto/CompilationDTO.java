package pl.lanuda.heks.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

public class CompilationDTO {

    private Long id;
    private String name;
    private boolean active;
    private LocalDateTime dueDate;
    private Set<GeekListDTO> geekLists;
    private Long totalGamesCount;
    private Long ratedGamesCount;
    private Long version;

    public CompilationDTO() {
    }

    public CompilationDTO(Long id, String name, boolean active, LocalDateTime dueDate, Set<GeekListDTO> geekLists, Long totalGamesCount, Long ratedGamesCount, Long version) {
        this.id = id;
        this.name = name;
        this.active = active;
        this.dueDate = dueDate;
        this.geekLists = geekLists;
        this.totalGamesCount = totalGamesCount;
        this.ratedGamesCount = ratedGamesCount;
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Set<GeekListDTO> getGeekLists() {
        return geekLists;
    }

    public void setGeekLists(Set<GeekListDTO> geekLists) {
        this.geekLists = geekLists;
    }

    public Long getTotalGamesCount() {
        return totalGamesCount;
    }

    public void setTotalGamesCount(Long totalGamesCount) {
        this.totalGamesCount = totalGamesCount;
    }

    public Long getRatedGamesCount() {
        return ratedGamesCount;
    }

    public void setRatedGamesCount(Long ratedGamesCount) {
        this.ratedGamesCount = ratedGamesCount;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompilationDTO)) return false;
        CompilationDTO that = (CompilationDTO) o;
        return active == that.active &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(totalGamesCount, that.totalGamesCount) &&
                Objects.equals(ratedGamesCount, that.ratedGamesCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, active, dueDate, totalGamesCount, ratedGamesCount);
    }
}

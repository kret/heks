package pl.lanuda.heks.dto;

import java.util.Objects;

public class GeekListDTO {

    private Long id;
    private String title;
    private String ownersUserName;
    private Long version;

    public GeekListDTO() {
    }

    public GeekListDTO(Long id, String title, String ownersUserName, Long version) {
        this.id = id;
        this.title = title;
        this.ownersUserName = ownersUserName;
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOwnersUserName() {
        return ownersUserName;
    }

    public void setOwnersUserName(String ownersUserName) {
        this.ownersUserName = ownersUserName;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeekListDTO)) return false;
        GeekListDTO that = (GeekListDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(ownersUserName, that.ownersUserName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, ownersUserName);
    }
}

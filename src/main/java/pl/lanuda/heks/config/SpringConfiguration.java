package pl.lanuda.heks.config;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StandardStaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;
import org.apache.camel.Exchange;
import org.apache.camel.spi.DataFormat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.web.client.RestTemplate;
import pl.lanuda.heks.parser.model.GameXML;
import pl.lanuda.heks.parser.model.GeekListXML;
import pl.lanuda.heks.service.BoardGameGeekDownloaderService;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Configuration
public class SpringConfiguration {

    private BoardGameGeekDownloaderService boardGameGeekDownloaderServiceInstance = null;
    private XStreamMarshaller marshallerInstance = null;

    @Bean
    public XStreamMarshaller marshaller() {
        if (marshallerInstance == null) {
            marshallerInstance = new XStreamMarshaller() {
                @Override
                protected XStream constructXStream() {
                    return new XStream() {
                        @Override
                        protected MapperWrapper wrapMapper(MapperWrapper next) {
                            return new MapperWrapper(next) {
                                @Override
                                public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                                    if (definedIn == Object.class) {
                                        return false;
                                    }
                                    return super.shouldSerializeMember(definedIn, fieldName);
                                }
                            };
                        }
                    };
                }
            };
            marshallerInstance.setStreamDriver(new StandardStaxDriver());
            marshallerInstance.setAnnotatedClasses(GeekListXML.class, GameXML.class);
        }
        return marshallerInstance;
    }

    @Bean
    public BoardGameGeekDownloaderService boardGameGeekDownloaderService() {
        if (boardGameGeekDownloaderServiceInstance == null) {
            boardGameGeekDownloaderServiceInstance = new BoardGameGeekDownloaderService();
        }
        return boardGameGeekDownloaderServiceInstance;
    }

    @Bean
    public DataFormat dataFormat() {
        return new DataFormat() {

            @Override
            public void marshal(Exchange exchange, Object graph, OutputStream stream) throws Exception {
            }

            @Override
            public Object unmarshal(Exchange exchange, InputStream stream) throws Exception {
                return marshaller().unmarshalInputStream(stream);
            }
        };
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = Optional.ofNullable(restTemplate.getMessageConverters())
            .orElse(new ArrayList<>());
        converters.add(marshallingHttpMessageConverter());
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }

    @Bean
    public MarshallingHttpMessageConverter marshallingHttpMessageConverter() {
        return new MarshallingHttpMessageConverter(marshaller(), marshaller());
    }
}

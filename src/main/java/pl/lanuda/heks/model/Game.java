package pl.lanuda.heks.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
public class Game {

    private Long id;
    private String name;
    private String type;
    private String thumbnail;
    private Short yearPublished;
    private Short minPlayers;
    private Short maxPlayers;
    private Short playingTime;
    private Short minPlayingTime;
    private Short maxPlayingTime;
    private Short minAge;
    private Double languageDependenceRatio;
    private String description;
//    private List<Person> designers;
//    private List<Person> artists;
    private List<GameVersion> versions = new ArrayList<>();
    private Set<Rating> ratings;
    private Set<GeekListItem> geekListItems;

    private LocalDateTime lastDownloadEnqueue;
    private LocalDateTime lastSuccessfulLoad;
    private Long version;


    public Game() {
    }

    public Game(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Short getYearPublished() {
        return yearPublished;
    }

    public void setYearPublished(Short yearPublished) {
        this.yearPublished = yearPublished;
    }

    public Short getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Short minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Short getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Short maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Short getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(Short playingTime) {
        this.playingTime = playingTime;
    }

    public Short getMinPlayingTime() {
        return minPlayingTime;
    }

    public void setMinPlayingTime(Short minPlayingTime) {
        this.minPlayingTime = minPlayingTime;
    }

    public Short getMaxPlayingTime() {
        return maxPlayingTime;
    }

    public void setMaxPlayingTime(Short maxPlayingTime) {
        this.maxPlayingTime = maxPlayingTime;
    }

    public Short getMinAge() {
        return minAge;
    }

    public void setMinAge(Short minAge) {
        this.minAge = minAge;
    }

    public Double getLanguageDependenceRatio() {
        return languageDependenceRatio;
    }

    public void setLanguageDependenceRatio(Double languageDependenceRatio) {
        this.languageDependenceRatio = languageDependenceRatio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = false, mappedBy = "game")
    public List<GameVersion> getVersions() {
        return versions;
    }

    public void setVersions(List<GameVersion> versions) {
        this.versions = versions;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "game")
    public Set<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    @OneToMany(cascade = {}, fetch = FetchType.LAZY, orphanRemoval = false, mappedBy = "game")
    public Set<GeekListItem> getGeekListItems() {
        return geekListItems;
    }

    public void setGeekListItems(Set<GeekListItem> geekListItems) {
        this.geekListItems = geekListItems;
    }

    public LocalDateTime getLastDownloadEnqueue() {
        return lastDownloadEnqueue;
    }

    public void setLastDownloadEnqueue(LocalDateTime lastDownloadEnqueue) {
        this.lastDownloadEnqueue = lastDownloadEnqueue;
    }

    public LocalDateTime getLastSuccessfulLoad() {
        return lastSuccessfulLoad;
    }

    public void setLastSuccessfulLoad(LocalDateTime lastSuccessfulLoad) {
        this.lastSuccessfulLoad = lastSuccessfulLoad;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

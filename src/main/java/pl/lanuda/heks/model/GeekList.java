package pl.lanuda.heks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
public class GeekList {

    private Long id;
    private LocalDateTime lastEditDate;
    private String ownersUserName;
    private String title;
    private List<GeekListItem> geekListItems;
    private LocalDateTime lastDownloadEnqueue;
    private LocalDateTime lastSuccessfulLoad;
    private Long version;

    public GeekList() {
    }

    public GeekList(Long id, LocalDateTime lastEditDate, String ownersUserName, String title) {
        this.id = id;
        this.lastEditDate = lastEditDate;
        this.ownersUserName = ownersUserName;
        this.title = title;
    }

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(LocalDateTime lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public String getOwnersUserName() {
        return ownersUserName;
    }

    public void setOwnersUserName(String ownersUserName) {
        this.ownersUserName = ownersUserName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIgnore
//    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "geekList", orphanRemoval = true)
    @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "geekList")//, orphanRemoval = true)
    public List<GeekListItem> getGeekListItems() {
        return geekListItems;
    }

    public void setGeekListItems(List<GeekListItem> geekListItems) {
        this.geekListItems = geekListItems;
    }

    @Transient
    public int getGeekListItemsCount() {
        return geekListItems == null ? 0 : geekListItems.size();
    }

    public LocalDateTime getLastDownloadEnqueue() {
        return lastDownloadEnqueue;
    }

    public void setLastDownloadEnqueue(LocalDateTime lastDownloadEnqueue) {
        this.lastDownloadEnqueue = lastDownloadEnqueue;
    }

    public LocalDateTime getLastSuccessfulLoad() {
        return lastSuccessfulLoad;
    }

    public void setLastSuccessfulLoad(LocalDateTime lastSuccessfulLoad) {
        this.lastSuccessfulLoad = lastSuccessfulLoad;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeekList)) return false;
        GeekList geekList = (GeekList) o;
        return Objects.equals(id, geekList.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

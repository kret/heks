package pl.lanuda.heks.model;

public enum RatingValue {

    NOT_RATED((short) 0),
    IGNORE((short) 1),
    LIKE((short) 2),
    WANT((short) 3),
    NEED((short) 4);

    private short value;

    RatingValue(short value) {
        this.value = value;
    }

    public short getValue() {
        return value;
    }
}

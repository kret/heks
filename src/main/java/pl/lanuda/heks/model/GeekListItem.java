package pl.lanuda.heks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class GeekListItem {

    private Long id;
    private String subType; // TODO: should this be an enum?
    private String ownerUserName; // name: author/creator userName ?
    private Long thumbsCount;
    private Long imageId;
    private String body;
    private GeekList geekList; // ManyToOne
    private Game game;
    private Long version;

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getOwnerUserName() {
        return ownerUserName;
    }

    public void setOwnerUserName(String ownerUserName) {
        this.ownerUserName = ownerUserName;
    }

    public Long getThumbsCount() {
        return thumbsCount;
    }

    public void setThumbsCount(Long thumbsCount) {
        this.thumbsCount = thumbsCount;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @JsonIgnore
    @ManyToOne(cascade = {}, fetch = FetchType.LAZY, optional = false)
    public GeekList getGeekList() {
        return geekList;
    }

    public void setGeekList(GeekList geekList) {
        this.geekList = geekList;
    }

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY, optional = false)
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "GeekListItem{" +
                "id=" + id +
                ", subType='" + subType + '\'' +
                ", ownerUserName='" + ownerUserName + '\'' +
                ", thumbsCount=" + thumbsCount +
                ", imageId=" + imageId +
                ", body='" + body + '\'' +
                ", geekList=" + geekList +
                ", game=" + game +
                '}';
    }
}

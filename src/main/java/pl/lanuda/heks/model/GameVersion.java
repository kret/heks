package pl.lanuda.heks.model;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.util.List;
import java.util.Set;

@Entity
public class GameVersion {

    private Long id;
    private Short yearPublished;
    private Game game;
    private Set<String> languages;
    private List<Publisher> publishers;
    private Long version;

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getYearPublished() {
        return yearPublished;
    }

    public void setYearPublished(Short yearPublished) {
        this.yearPublished = yearPublished;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY, optional = false)
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @ElementCollection
    @CollectionTable(name = "language", joinColumns = @JoinColumn(name = "game_version_id"))
    @Column(name = "name")
    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "game_version_publisher",
            joinColumns = @JoinColumn(name = "game_version_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "publisher_id", referencedColumnName = "id"))
    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}

package pl.lanuda.heks.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Version;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Compilation {

    private Long id;
    private String name;
    private boolean active;
    private LocalDateTime dueDate;
    private List<GeekList> geekLists;
    private Long version;

    public Compilation() {
        this(null, true, null);
    }

    public Compilation(String name, boolean active, LocalDateTime dueDate) {
        this.active = active;
        this.name = name;
        this.dueDate = dueDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    public List<GeekList> getGeekLists() {
        return geekLists;
    }

    public void setGeekLists(List<GeekList> geekLists) {
        this.geekLists = geekLists;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}

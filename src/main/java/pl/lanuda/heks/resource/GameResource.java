package pl.lanuda.heks.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lanuda.heks.dto.RatingDTO;
import pl.lanuda.heks.model.Rating;
import pl.lanuda.heks.service.GameService;

@RestController
@RequestMapping("/api/game")
public class GameResource {

    private final GameService gameService;

    @Autowired
    public GameResource(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/{gameId}/rating")
    public RatingDTO rate(@PathVariable("gameId") Long gameId, @RequestBody Rating inputRating, Authentication authentication) {
        Rating rating = gameService.rate(gameId, inputRating, authentication.getName());
        return new RatingDTO(rating.getRater().getUsername(), rating.getValue(), rating.getComment(), rating.getVersion());
    }
}

package pl.lanuda.heks.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lanuda.heks.dto.CompilationDTO;
import pl.lanuda.heks.dto.GameDTO;
import pl.lanuda.heks.dto.GeekListDTO;
import pl.lanuda.heks.exception.DuplicateGeekListInCompilationException;
import pl.lanuda.heks.model.Compilation;
import pl.lanuda.heks.model.Game;
import pl.lanuda.heks.model.GeekList;
import pl.lanuda.heks.model.GeekListItem;
import pl.lanuda.heks.model.Publisher;
import pl.lanuda.heks.model.Rating;
import pl.lanuda.heks.model.RatingValue;
import pl.lanuda.heks.repository.CompilationRepository;
import pl.lanuda.heks.repository.GameRepository;
import pl.lanuda.heks.repository.GeekListRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/compilation")
public class CompilationResource {

    private final CompilationRepository compilationRepository;
    private final GeekListRepository geekListRepository;
    private final GameRepository gameRepository;

    @Autowired
    public CompilationResource(CompilationRepository compilationRepository, GeekListRepository geekListRepository, GameRepository gameRepository) {
        this.compilationRepository = compilationRepository;
        this.geekListRepository = geekListRepository;
        this.gameRepository = gameRepository;
    }

    @GetMapping
    public Set<CompilationDTO> all(Authentication authentication) {
        List<Compilation> allCompilations = compilationRepository.findAll();
        return allCompilations.stream()
                .map(compilation -> buildCompilationDTO(compilation, authentication.getName()))
                .collect(Collectors.toSet());
    }

    @GetMapping
    @RequestMapping("/{compilationId}")
    public CompilationDTO getOne(@PathVariable("compilationId") Long compilationId, Authentication authentication) {
        Compilation compilation = compilationRepository.findOne(compilationId);
        return buildCompilationDTO(compilation, authentication.getName());
    }

    @PostMapping
    public CompilationDTO createCompilation(@RequestBody CompilationDTO dto, Authentication authentication) {
        Compilation compilation = new Compilation();
        compilation.setName(dto.getName());
        compilation.setDueDate(dto.getDueDate());
        Compilation createdCompilation = compilationRepository.save(compilation);
        return buildCompilationDTO(createdCompilation, authentication.getName());
    }

    @PostMapping
    @RequestMapping("/{compilationId}/list")
    public CompilationDTO addGeekListToCompilation(@PathVariable("compilationId") Long compilationId,
                                                   @RequestBody GeekListDTO geekListDTO,
                                                   Authentication authentication) {

        GeekList geekList = geekListRepository.findOne(geekListDTO.getId());
        if (geekList == null) {
            geekList = new GeekList();
            geekList.setId(geekListDTO.getId());
        }
        Compilation compilation = compilationRepository.findOne(compilationId);
        List<GeekList> geekLists = compilation.getGeekLists();
        if (geekLists == null) {
            geekLists = new ArrayList<>();
            compilation.setGeekLists(geekLists);
        }
        if (geekLists.contains(geekList)) {
            throw new DuplicateGeekListInCompilationException(geekList.getId(), compilationId);
        }
        geekLists.add(geekList);
        Compilation savedCompilation = compilationRepository.save(compilation);
        return buildCompilationDTO(savedCompilation, authentication.getName());
    }

    @DeleteMapping
    @RequestMapping("/{compilationId}/list/{geekListId}")
    public CompilationDTO removeGeekListFromCompilation(@PathVariable("compilationId") Long compilationId,
                                                        @PathVariable("geekListId") Long geekListId,
                                                        Authentication authentication) {

        Compilation compilation = compilationRepository.findOne(compilationId);
        compilation.getGeekLists().removeIf(geekList -> geekList.getId().equals(geekListId));
        Compilation updatedCompilation = compilationRepository.save(compilation);
        return buildCompilationDTO(updatedCompilation, authentication.getName());
    }

    @GetMapping
    @RequestMapping("/{compilationId}/games")
    public Set<GameDTO> getCompilationGames(@PathVariable("compilationId") Long compilationId,
                                            @PageableDefault(sort = {"name"}) Pageable pageable,
                                            Authentication authentication) {

        List<Game> games = gameRepository.findGamesForCompilation(compilationId, pageable);
        return games.stream().map(game -> {
            GameDTO gameDTO = new GameDTO(game.getId());
            gameDTO.setName(game.getName());
            gameDTO.setType(game.getType());
            gameDTO.setThumbnail(game.getThumbnail());
            gameDTO.setYearPublished(game.getYearPublished());
            gameDTO.setMinPlayers(game.getMinPlayers());
            gameDTO.setMaxPlayers(game.getMaxPlayers());
            gameDTO.setMinPlayingTime(game.getMinPlayingTime());
            gameDTO.setMaxPlayingTime(game.getMaxPlayingTime());
            gameDTO.setPlayingTime(game.getPlayingTime());
            gameDTO.setMinAge(game.getMinAge());
            gameDTO.setLanguageDependenceRatio(game.getLanguageDependenceRatio());
            gameDTO.setDescription(game.getDescription());
            gameDTO.setVersion(game.getVersion());

            GameDTO finalGameDTO = gameDTO;
            game.getVersions().forEach(gameVersion -> {
                finalGameDTO.addGameVersion(
                        gameVersion.getYearPublished(),
                        gameVersion.getPublishers().stream().map(Publisher::getName).collect(Collectors.toSet()),
                        gameVersion.getLanguages(),
                        gameVersion.getVersion()
                );
            });
            game.getRatings().forEach(rating -> {
                finalGameDTO.addGameRating(
                        rating.getRater().getUsername(),
                        rating.getValue(),
                        rating.getComment(),
                        rating.getRater().getUsername().equals(authentication.getName()),
                        rating.getVersion()
                );
            });
            game.getGeekListItems().forEach(geekListItem -> {
                gameDTO.addGeekListItem(geekListItem.getGeekList().getId(), geekListItem.getId());
            });
            return gameDTO;
        }).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private CompilationDTO buildCompilationDTO(Compilation compilation, String username) {
        List<GeekList> geekLists = Optional.ofNullable(compilation.getGeekLists()).orElse(new ArrayList<>());
        return new CompilationDTO(
                compilation.getId(),
                compilation.getName(),
                compilation.getActive(),
                compilation.getDueDate(),
                geekLists.stream().map(geekList -> new GeekListDTO(
                        geekList.getId(),
                        geekList.getTitle(),
                        geekList.getOwnersUserName(),
                        geekList.getVersion()
                ))
                        .collect(Collectors.toSet()),
                geekLists.stream()
                        .flatMap(geekList -> Optional.ofNullable(geekList.getGeekListItems()).orElse(new ArrayList<>()).stream()
                                .map(GeekListItem::getGame)
                                .map(Game::getId))
                        .distinct()
                        .count(),
                geekLists.stream()
                        .flatMap(geekList -> Optional.ofNullable(geekList.getGeekListItems()).orElse(new ArrayList<>()).stream()
                                .map(GeekListItem::getGame)
                                .flatMap(game -> Optional.ofNullable(game.getRatings()).orElse(new HashSet<>()).stream()
                                        .filter(rating -> rating.getValue() != RatingValue.NOT_RATED &&
                                                rating.getRater().getUsername().equals(username))
                                       .map(Rating::getId)))
                        .distinct()
                        .count(),
                compilation.getVersion()
        );
    }
}

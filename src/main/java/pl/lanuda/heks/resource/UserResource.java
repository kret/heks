package pl.lanuda.heks.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lanuda.heks.dto.UserRegistrationDTO;
import pl.lanuda.heks.model.User;
import pl.lanuda.heks.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserResource {

    private final UserService userService;

    @Autowired
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public User register(@RequestBody UserRegistrationDTO userRegistrationData) {
        return userService.register(userRegistrationData);
    }
}

package pl.lanuda.heks.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DuplicateGeekListInCompilationException extends RuntimeException {

    public DuplicateGeekListInCompilationException(Long geekListId, Long compilationId) {
        super("Duplicate GeekList " + geekListId + " in Compilation " + compilationId);
    }
}

package pl.lanuda.heks.parser.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import pl.lanuda.heks.parser.converter.LocalDateTimeTimestampConverter;

import java.time.LocalDateTime;
import java.util.List;

@XStreamAlias("geeklist")
public class GeekListXML {

    @XStreamAsAttribute
    private Long id;

    @XStreamAlias("editdate")
    @XStreamConverter(LocalDateTimeTimestampConverter.class)
    private LocalDateTime lastEditDate;

    @XStreamAlias("username")
    private String ownersUserName;

    private String title;

    @XStreamImplicit(itemFieldName = "item")
    private List<GeekListItemXML> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(LocalDateTime lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public String getOwnersUserName() {
        return ownersUserName;
    }

    public void setOwnersUserName(String ownersUserName) {
        this.ownersUserName = ownersUserName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<GeekListItemXML> getItems() {
        return items;
    }

    public void setItems(List<GeekListItemXML> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "GeekListXML{" +
                "id=" + id +
                ", lastEditDate=" + lastEditDate +
                ", ownersUserName='" + ownersUserName + '\'' +
                ", title='" + title + '\'' +
                ", items=" + items +
                '}';
    }


    public static class GeekListItemXML {

        @XStreamAlias("id")
        @XStreamAsAttribute
        private Long geekListItemId;

        @XStreamAlias("subtype")
        @XStreamAsAttribute
        private String subType;

        @XStreamAlias("objectid")
        @XStreamAsAttribute
        private Long objectId;

        @XStreamAlias("objectname")
        @XStreamAsAttribute
        private String objectName;

        @XStreamAlias("username")
        @XStreamAsAttribute
        private String ownerUserName;

        @XStreamAlias("thumbs")
        @XStreamAsAttribute
        private Long thumbsCount;

        @XStreamAlias("imageid")
        @XStreamAsAttribute
        private Long imageId;

        private String body;

        public Long getGeekListItemId() {
            return geekListItemId;
        }

        public void setGeekListItemId(Long geekListItemId) {
            this.geekListItemId = geekListItemId;
        }

        public String getSubType() {
            return subType;
        }

        public void setSubType(String subType) {
            this.subType = subType;
        }

        public Long getObjectId() {
            return objectId;
        }

        public void setObjectId(Long objectId) {
            this.objectId = objectId;
        }

        public String getObjectName() {
            return objectName;
        }

        public void setObjectName(String objectName) {
            this.objectName = objectName;
        }

        public String getOwnerUserName() {
            return ownerUserName;
        }

        public void setOwnerUserName(String ownerUserName) {
            this.ownerUserName = ownerUserName;
        }

        public Long getThumbsCount() {
            return thumbsCount;
        }

        public void setThumbsCount(Long thumbsCount) {
            this.thumbsCount = thumbsCount;
        }

        public Long getImageId() {
            return imageId;
        }

        public void setImageId(Long imageId) {
            this.imageId = imageId;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        @Override
        public String toString() {
            return "GeekListItemXML{" +
                    "geekListItemId=" + geekListItemId +
                    ", subType='" + subType + '\'' +
                    ", objectId=" + objectId +
                    ", objectName='" + objectName + '\'' +
                    ", ownerUserName='" + ownerUserName + '\'' +
                    ", thumbsCount=" + thumbsCount +
                    ", imageId=" + imageId +
                    ", body='" + body + '\'' +
                    '}';
        }
    }
}

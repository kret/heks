package pl.lanuda.heks.parser.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("items")
public class GameXML {
// http://www.boardgamegeek.com/xmlapi2/thing?id=181971&versions=1&videos=1

    @XStreamImplicit(itemFieldName = "item")
    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "GameXML{" +
                "items=" + items +
                '}';
    }

    public static class Item {

        @XStreamAsAttribute
        private Long id;

        @XStreamAsAttribute
        private String type; // TODO should be an enum / different entity ???

        private String thumbnail;

        @XStreamImplicit(itemFieldName = "name")
        private List<Name> names;

        private String description;

        @XStreamAlias("yearpublished")
        private FieldWithShortValue yearPublished;

        @XStreamAlias("minplayers")
        private FieldWithShortValue minPlayers;

        @XStreamAlias("maxplayers")
        private FieldWithShortValue maxPlayers;

        @XStreamAlias("playingtime")
        private FieldWithShortValue playingTime;

        @XStreamAlias("minplaytime")
        private FieldWithShortValue minPlayingTime;

        @XStreamAlias("maxplaytime")
        private FieldWithShortValue maxPlayingTime;

        @XStreamAlias("minage")
        private FieldWithShortValue minAge;

        @XStreamImplicit(itemFieldName = "poll")
        private List<Poll> polls;

        @XStreamImplicit(itemFieldName = "link")
        private List<Link> links;

        private Versions versions;

        // TODO add game designers
        // TODO add expansions???
        // TODO add videos


        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public List<Name> getNames() {
            return names;
        }

        public void setNames(List<Name> names) {
            this.names = names;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public FieldWithShortValue getYearPublished() {
            return yearPublished;
        }

        public void setYearPublished(FieldWithShortValue yearPublished) {
            this.yearPublished = yearPublished;
        }

        public FieldWithShortValue getMinPlayers() {
            return minPlayers;
        }

        public void setMinPlayers(FieldWithShortValue minPlayers) {
            this.minPlayers = minPlayers;
        }

        public FieldWithShortValue getMaxPlayers() {
            return maxPlayers;
        }

        public void setMaxPlayers(FieldWithShortValue maxPlayers) {
            this.maxPlayers = maxPlayers;
        }

        public FieldWithShortValue getPlayingTime() {
            return playingTime;
        }

        public void setPlayingTime(FieldWithShortValue playingTime) {
            this.playingTime = playingTime;
        }

        public FieldWithShortValue getMinPlayingTime() {
            return minPlayingTime;
        }

        public void setMinPlayingTime(FieldWithShortValue minPlayingTime) {
            this.minPlayingTime = minPlayingTime;
        }

        public FieldWithShortValue getMaxPlayingTime() {
            return maxPlayingTime;
        }

        public void setMaxPlayingTime(FieldWithShortValue maxPlayingTime) {
            this.maxPlayingTime = maxPlayingTime;
        }

        public FieldWithShortValue getMinAge() {
            return minAge;
        }

        public void setMinAge(FieldWithShortValue minAge) {
            this.minAge = minAge;
        }

        public List<Poll> getPolls() {
            return polls;
        }

        public void setPolls(List<Poll> polls) {
            this.polls = polls;
        }

        public List<Link> getLinks() {
            return links;
        }

        public void setLinks(List<Link> links) {
            this.links = links;
        }

        public Versions getVersions() {
            return versions;
        }

        public void setVersions(Versions versions) {
            this.versions = versions;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "id=" + id +
                    ", type='" + type + '\'' +
                    ", thumbnail='" + thumbnail + '\'' +
                    ", names=" + names +
                    ", description='" + description + '\'' +
                    ", yearPublished=" + yearPublished +
                    ", minPlayers=" + minPlayers +
                    ", maxPlayers=" + maxPlayers +
                    ", playingTime=" + playingTime +
                    ", minPlayingTime=" + minPlayingTime +
                    ", maxPlayingTime=" + maxPlayingTime +
                    ", minAge=" + minAge +
                    ", polls=" + polls +
                    ", links=" + links +
                    ", versions=" + versions +
                    '}';
        }

        public static class FieldWithShortValue {

            @XStreamAsAttribute
            private Short value;

            public Short getValue() {
                return value;
            }

            public void setValue(Short value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return "FieldWithShortValue{" +
                        "value=" + value +
                        '}';
            }
        }

        public static class Link {

            @XStreamAsAttribute
            private Long id;

            @XStreamAsAttribute
            private String type;

            @XStreamAsAttribute
            private String value;

            public Long getId() {
                return id;
            }

            public void setId(Long id) {
                this.id = id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return "Link{" +
                        "id=" + id +
                        ", type='" + type + '\'' +
                        ", value='" + value + '\'' +
                        '}';
            }
        }
    }

    public static class Versions {

        @XStreamImplicit(itemFieldName = "item")
        private List<Item> items;

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        @Override
        public String toString() {
            return "Versions{" +
                    "items=" + items +
                    '}';
        }
    }

    public static class Name {

        @XStreamAsAttribute
        private String type;

        @XStreamAlias("sortindex")
        @XStreamAsAttribute
        private Short sortIndex;

        @XStreamAsAttribute
        private String value;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Short getSortIndex() {
            return sortIndex;
        }

        public void setSortIndex(Short sortIndex) {
            this.sortIndex = sortIndex;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "type='" + type + '\'' +
                    ", sortIndex=" + sortIndex +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

    public static class Poll {

        @XStreamAsAttribute
        private String name;

        @XStreamAsAttribute
        private String title;

        @XStreamAlias("totalvotes")
        @XStreamAsAttribute
        private Long totalVotes;

        @XStreamImplicit(itemFieldName = "results")
        private List<Results> results;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Long getTotalVotes() {
            return totalVotes;
        }

        public void setTotalVotes(Long totalVotes) {
            this.totalVotes = totalVotes;
        }

        public List<Results> getResults() {
            return results == null ?  new ArrayList<>() : results;
        }

        public void setResults(List<Results> results) {
            this.results = results;
        }

        @Override
        public String toString() {
            return "Poll{" +
                    "name='" + name + '\'' +
                    ", title='" + title + '\'' +
                    ", totalVotes=" + totalVotes +
                    ", results=" + results +
                    '}';
        }

        public static class Results {

            @XStreamAlias("numplayers")
            @XStreamAsAttribute
            private String numOfPlayers;

            @XStreamImplicit(itemFieldName = "result")
            private List<Result> results;

            public String getNumOfPlayers() {
                return numOfPlayers;
            }

            public void setNumOfPlayers(String numOfPlayers) {
                this.numOfPlayers = numOfPlayers;
            }

            public List<Result> getResults() {
                return results;
            }

            public void setResults(List<Result> results) {
                this.results = results;
            }

            @Override
            public String toString() {
                return "Results{" +
                        "numOfPlayers=" + numOfPlayers +
                        ", results=" + results +
                        '}';
            }

            public static class Result {

                @XStreamAsAttribute
                private String value;

                @XStreamAlias("numvotes")
                @XStreamAsAttribute
                private Long numVotes;

                @XStreamAsAttribute
                private Short level;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public Long getNumVotes() {
                    return numVotes;
                }

                public void setNumVotes(Long numVotes) {
                    this.numVotes = numVotes;
                }

                public Short getLevel() {
                    return level;
                }

                public void setLevel(Short level) {
                    this.level = level;
                }

                @Override
                public String toString() {
                    return "Result{" +
                            "value='" + value + '\'' +
                            ", numVotes=" + numVotes +
                            ", level=" + level +
                            '}';
                }
            }
        }
    }
}

package pl.lanuda.heks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.lanuda.heks.model.GameVersion;

@Repository
public interface GameVersionRepository extends JpaRepository<GameVersion, Long> {
}

package pl.lanuda.heks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.lanuda.heks.model.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

    @Query("select r from Rating r where r.game.id = :gameId and r.rater.username = :username")
    Rating findByUserAndGame(@Param("username") String username, @Param("gameId") Long gameId);
}

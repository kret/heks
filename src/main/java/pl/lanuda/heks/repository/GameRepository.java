package pl.lanuda.heks.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.lanuda.heks.model.Game;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

    // TODO use Spring Data methods?
    @Query(value = "select distinct ag from Game ag where exists (select g from Compilation c join c.geekLists gl join gl.geekListItems gli join gli.game g where c.id = :compilationId and g.id = ag.id)")
    List<Game> findGamesForCompilation(@Param("compilationId") Long compilationId, Pageable pageable);

    @Query(value = "select distinct on (g.id) g.* from compilation c " +
            "left outer join compilation_geek_lists gcl on c.id = gcl.compilation_id " +
            "left outer join geek_list gl on gl.id = gcl.geek_lists_id " +
            "left outer join geek_list_item gli on gli.geek_list_id = gl.id " +
            "left outer join game g on g.id = gli.game_id " +
            "where " +
            "c.active = 't' and " +
            "c.due_date > now() and " +
            "( " +
            "g.type is null or " +
            "g.thumbnail is null or " +
            "g.min_players is null or " +
            "g.max_players is null or " +
            "g.playing_time is null or " +
            "g.min_playing_time is null or " +
            "g.max_playing_time is null or " +
            "g.min_age is null or " +
            "g.description is null " +
            ") and " +
            "( " +
                "( " +
                    "g.last_download_enqueue + (c.due_date - now()) / 10 < now() and " +
                    "g.last_successful_load + (c.due_date - now()) / 10 < now() " +
                ") or " +
                "( " +
                    "g.last_download_enqueue is null and " +
                    "g.last_successful_load is null " +
                ") " +
            ") " +
            "and g.id is not null " +
            "limit 10",
            nativeQuery = true)
    List<Game> findGamesForRefresh();
}

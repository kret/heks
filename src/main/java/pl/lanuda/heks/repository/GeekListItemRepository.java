package pl.lanuda.heks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.lanuda.heks.model.GeekListItem;

@Repository
public interface GeekListItemRepository extends JpaRepository<GeekListItem, Long> {
}

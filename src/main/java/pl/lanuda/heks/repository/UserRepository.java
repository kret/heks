package pl.lanuda.heks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.lanuda.heks.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
}

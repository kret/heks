package pl.lanuda.heks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.lanuda.heks.model.GeekList;

import java.util.List;

@Repository
public interface GeekListRepository extends JpaRepository<GeekList, Long> {

    @Query(value = "select distinct on (gl.id) gl.* from compilation c " +
            "left outer join compilation_geek_lists gcl on c.id = gcl.compilation_id " +
            "left outer join geek_list gl on gl.id = gcl.geek_lists_id " +
            "where " +
            "c.active = 't' and " +
            "c.due_date > now() and " +
            "( " +
            "( " +
            "gl.last_download_enqueue + (c.due_date - now()) / 10 < now() and " +
            "gl.last_successful_load + (c.due_date - now()) / 10 < now() " +
            ") or ( " +
            "gl.last_download_enqueue is null and " +
            "gl.last_successful_load is null " +
            ") " +
            ") " +
            "and gl.id is not null",
            nativeQuery = true)
    List<GeekList> findGeekListsForRefresh();
}

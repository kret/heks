package pl.lanuda.heks.service.input;

public abstract class BoardGameGeekDownloadTaskInput {

    protected static final String BGG_BASE_DOMAIN = "https://www.boardgamegeek.com/";
    protected static final String OUTPUT_PATH_ROOT = "esb";

    public abstract String getBGGUrl();

    public abstract String getBasePath();
    public abstract String getOutputFileName();
    public abstract String getTemporaryFileName();
}

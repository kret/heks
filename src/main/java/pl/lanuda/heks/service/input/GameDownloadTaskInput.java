package pl.lanuda.heks.service.input;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Collectors;

public class GameDownloadTaskInput extends BoardGameGeekDownloadTaskInput {

    private static final String BGG_BASE_CONTEXT_PATH = "/xmlapi2/thing?versions=1&videos=1&id=";
    protected static final String OUTPUT_PATH = "thing-import";
    private Set<Long> inputIds;
    private String timeDataForFileName;

    public GameDownloadTaskInput(Set<Long> inputIds) {
        this.inputIds = inputIds;
        this.timeDataForFileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"));
    }

    @Override
    public String getBGGUrl() {
        return BGG_BASE_DOMAIN + BGG_BASE_CONTEXT_PATH + inputIds.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    @Override
    public String getBasePath() {
        return Paths.get(OUTPUT_PATH_ROOT, OUTPUT_PATH).toString();
    }

    @Override
    public String getOutputFileName() {
        return "game_" + timeDataForFileName
                + "_" + inputIds.stream().map(Object::toString).collect(Collectors.joining("_"))
                + ".xml";
    }

    @Override
    public String getTemporaryFileName() {
        return "game_" + timeDataForFileName
                + "_" + inputIds.stream().map(Object::toString).collect(Collectors.joining("_"))
                + ".tmp";
    }
}

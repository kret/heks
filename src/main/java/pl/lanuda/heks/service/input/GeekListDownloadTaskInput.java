package pl.lanuda.heks.service.input;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GeekListDownloadTaskInput extends BoardGameGeekDownloadTaskInput {

    private static final String BGG_BASE_CONTEXT_PATH = "xmlapi/geeklist/";
    protected static final String OUTPUT_PATH = "geek-list-import";
    private Long inputId;
    private String timeDataForFileName;

    public GeekListDownloadTaskInput(Long id) {
        this.inputId = id;
        this.timeDataForFileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"));
    }

    @Override
    public String getBGGUrl() {
        return BGG_BASE_DOMAIN + BGG_BASE_CONTEXT_PATH + inputId;
    }

    @Override
    public String getBasePath() {
        return Paths.get(OUTPUT_PATH_ROOT, OUTPUT_PATH).toString();
    }

    @Override
    public String getOutputFileName() {
        return "geek-list_" + timeDataForFileName
                + "_" + inputId
                + ".xml";
    }

    @Override
    public String getTemporaryFileName() {
        return "geek-list_" + timeDataForFileName
                + "_" + inputId
                + ".tmp";
    }
}

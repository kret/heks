package pl.lanuda.heks.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lanuda.heks.model.Game;
import pl.lanuda.heks.model.GeekList;
import pl.lanuda.heks.model.GeekListItem;
import pl.lanuda.heks.parser.model.GeekListXML;
import pl.lanuda.heks.repository.GameRepository;
import pl.lanuda.heks.repository.GeekListItemRepository;
import pl.lanuda.heks.repository.GeekListRepository;
import pl.lanuda.heks.service.input.GeekListDownloadTaskInput;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GeekListService {

    private final GeekListRepository geekListRepository;
    private final GeekListItemRepository geekListItemRepository;
    private final GameRepository gameRepository;
    private final BoardGameGeekDownloaderService downloaderService;

    @Autowired
    public GeekListService(GeekListRepository geekListRepository, GeekListItemRepository geekListItemRepository, GameRepository gameRepository, BoardGameGeekDownloaderService downloaderService) {
        this.geekListRepository = geekListRepository;
        this.geekListItemRepository = geekListItemRepository;
        this.gameRepository = gameRepository;
        this.downloaderService = downloaderService;
    }

    @Transactional
    public void findGeekListsForRefresh() {
        List<GeekList> geekLists = geekListRepository.findGeekListsForRefresh();

        geekLists.forEach(geekList -> {
            downloaderService.enqueue(new GeekListDownloadTaskInput(geekList.getId()));
            geekList.setLastDownloadEnqueue(LocalDateTime.now());
            geekListRepository.save(geekList);
        });
    }

    @Transactional
    public void refreshGeekListFromXML(GeekListXML geekListXML) {
        // get list from db
        GeekList geekList = geekListRepository.findOne(geekListXML.getId());
        // update its fields
        if (geekList != null) {
            geekList.setTitle(geekListXML.getTitle());
            geekList.setOwnersUserName(geekListXML.getOwnersUserName());
            geekList.setLastEditDate(geekListXML.getLastEditDate());

            List<GeekListXML.GeekListItemXML> geekListItemXMLs = geekListXML.getItems();
            if (geekListItemXMLs != null) {
                List<GeekListItem> geekListItems = geekListItemXMLs.stream()
                        .filter(geekListItemXML -> "boardgame".equals(geekListItemXML.getSubType()) || "boardgameexpansion".equals(geekListItemXML.getSubType()))
                        .map(geekListItemXML -> {
                            GeekListItem geekListItem = geekListItemRepository.findOne(geekListItemXML.getGeekListItemId());
                            if (geekListItem == null) {
                                geekListItem = new GeekListItem();
                                geekListItem.setId(geekListItemXML.getGeekListItemId());
                                geekListItem.setGeekList(geekList);

                                //   associate with game
                                Game game = gameRepository.findOne(geekListItemXML.getObjectId());
                                if (game == null) {
                                    game = new Game(geekListItemXML.getObjectId(), geekListItemXML.getObjectName());
                                    game = gameRepository.save(game);
                                }
                                geekListItem.setGame(game);
                                geekListItemRepository.save(geekListItem);
                            }
                            geekListItem.setBody(geekListItemXML.getBody());
                            geekListItem.setImageId(geekListItemXML.getImageId());
                            geekListItem.setOwnerUserName(geekListItemXML.getOwnerUserName());
                            geekListItem.setSubType(geekListItemXML.getSubType());
                            geekListItem.setThumbsCount(geekListItemXML.getThumbsCount());

                            return geekListItem;
                        })
                        .collect(Collectors.toList());
                geekList.setGeekListItems(geekListItems);
                geekList.setLastSuccessfulLoad(LocalDateTime.now());
            }
            geekListRepository.save(geekList);
        }
    }
}

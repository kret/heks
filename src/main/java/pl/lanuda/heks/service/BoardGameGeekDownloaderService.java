package pl.lanuda.heks.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.lanuda.heks.service.input.BoardGameGeekDownloadTaskInput;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class BoardGameGeekDownloaderService {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private List<BoardGameGeekDownloadTaskInput> queue = new ArrayList<>();

    public void download() {
        if (!queue.isEmpty()) {
            BoardGameGeekDownloadTaskInput task = queue.remove(0);

            try {
                String tempOutputPath = Paths.get(task.getBasePath(), task.getTemporaryFileName()).toString();
                URL bggURL = new URL(task.getBGGUrl());
                ReadableByteChannel rbc = Channels.newChannel(bggURL.openStream());
                FileOutputStream fos = new FileOutputStream(tempOutputPath);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

                Path source = Paths.get(tempOutputPath);
                Files.move(source, source.resolveSibling(task.getOutputFileName()));
            } catch (MalformedURLException e) {
                logger.warn("Invalid URL to download from BGG ({}), skipping. {}", task.getBGGUrl(), e);
            } catch (FileNotFoundException e) {
                logger.error("Problem with output file {}, {}", task.getTemporaryFileName(), e);
            } catch (IOException e) {
                logger.error("IOException, {}", e);
            }
        }
    }

    public boolean enqueue(BoardGameGeekDownloadTaskInput task) {
        return queue.add(task);
    }
}

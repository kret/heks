package pl.lanuda.heks.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.lanuda.heks.model.Game;
import pl.lanuda.heks.model.GameVersion;
import pl.lanuda.heks.model.Publisher;
import pl.lanuda.heks.model.Rating;
import pl.lanuda.heks.parser.model.GameXML;
import pl.lanuda.heks.repository.GameRepository;
import pl.lanuda.heks.repository.GameVersionRepository;
import pl.lanuda.heks.repository.PublisherRepository;
import pl.lanuda.heks.repository.RatingRepository;
import pl.lanuda.heks.repository.UserRepository;
import pl.lanuda.heks.service.input.GameDownloadTaskInput;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GameService {

    private final GameRepository gameRepository;
    private final GameVersionRepository gameVersionRepository;
    private final RatingRepository ratingRepository;
    private final UserRepository userRepository;
    private final PublisherRepository publisherRepository;
    private final BoardGameGeekDownloaderService boardGameGeekDownloaderService;

    @Autowired
    public GameService(GameRepository gameRepository, GameVersionRepository gameVersionRepository, RatingRepository ratingRepository, UserRepository userRepository, PublisherRepository publisherRepository, BoardGameGeekDownloaderService boardGameGeekDownloaderService) {
        this.gameRepository = gameRepository;
        this.gameVersionRepository = gameVersionRepository;
        this.ratingRepository = ratingRepository;
        this.userRepository = userRepository;
        this.publisherRepository = publisherRepository;
        this.boardGameGeekDownloaderService = boardGameGeekDownloaderService;
    }

    @Transactional
    public void findGamesForRefresh() {
        List<Game> games = gameRepository.findGamesForRefresh();
        if (!games.isEmpty()) {
            boardGameGeekDownloaderService.enqueue(
                    new GameDownloadTaskInput(
                            games.stream()
                                    .limit(10)
                                    .map(Game::getId)
                                    .collect(Collectors.toSet())
                    )
            );
            games.forEach(game -> {
                game.setLastDownloadEnqueue(LocalDateTime.now());
                gameRepository.save(game);
            });
        }
    }

    @Transactional
    public void refreshGameFromXML(GameXML gameXML) {
        gameXML.getItems().forEach(item -> {
            Game game = gameRepository.findOne(item.getId());
            if (game != null) {
                game.setId(item.getId());
                game.setName(item.getNames().stream().filter(name -> "primary".equals(name.getType())).findFirst().get().getValue());
                if (item.getThumbnail() != null) {
                    game.setThumbnail(item.getThumbnail().replace("_t", "_sq"));
                }
                game.setType(item.getType());
                game.setYearPublished(item.getYearPublished().getValue());
                game.setMinPlayers(item.getMinPlayers().getValue());
                game.setMaxPlayers(item.getMaxPlayers().getValue());
                game.setPlayingTime(item.getPlayingTime().getValue());
                game.setMinPlayingTime(item.getMinPlayingTime().getValue());
                game.setMaxPlayingTime(item.getMaxPlayingTime().getValue());
                game.setMinAge(item.getMinAge().getValue());
                game.setDescription(item.getDescription());

                GameXML.Poll languageDependencePoll = item.getPolls().stream().filter(poll -> "language_dependence".equals(poll.getName())).findFirst().get();
                long ratio = 0;
                long votesCount = 0;
                short level = 1;
                if (!languageDependencePoll.getResults().isEmpty()) {
                    for (GameXML.Poll.Results.Result result : languageDependencePoll.getResults().get(0).getResults()) {
                        votesCount += result.getNumVotes();
                        ratio += result.getNumVotes() * level;
                        level += 1;
                    }
                    if (votesCount > 0) {
                        game.setLanguageDependenceRatio((double) ratio / votesCount);
                    }
                }
                List<GameVersion> versions = new ArrayList<>();
                item.getVersions().getItems().forEach(version -> {

                    GameVersion gameVersion = gameVersionRepository.findOne(version.getId());
                    if (gameVersion == null) {
                        gameVersion = new GameVersion();
                        gameVersion.setId(version.getId());
                        gameVersion.setPublishers(new ArrayList<>());
                        gameVersion.setGame(game);
                    }

                    gameVersion.setYearPublished(version.getYearPublished().getValue());
                    List<Publisher> publishers = gameVersion.getPublishers();

                    List<GameXML.Item.Link> publishersFromXML = version.getLinks().stream()
                            .filter(link -> "boardgamepublisher".equals(link.getType()))
                            .collect(Collectors.toList());

                    publishersFromXML.forEach(link -> addPublisherToGameVersion(publishers, link));

                    Set<String> languagesFromXML = version.getLinks().stream()
                            .filter(link -> "language".equals(link.getType()))
                            .map(GameXML.Item.Link::getValue)
                            .collect(Collectors.toSet());
                    gameVersion.setLanguages(languagesFromXML);
                    versions.add(gameVersion);
                });
                game.setVersions(versions);
                game.setLastSuccessfulLoad(LocalDateTime.now());

                gameRepository.save(game);
            }
        });
    }

    @Transactional
    public Rating rate(Long gameId, Rating inputRating, String username) {
        Rating rating = ratingRepository.findByUserAndGame(username, gameId);
        if (rating == null) {
            rating = new Rating();
            rating.setGame(gameRepository.findOne(gameId));
            rating.setRater(userRepository.findOne(username));
        } else {
            rating.setVersion(inputRating.getVersion());
        }
        rating.setValue(inputRating.getValue());
        rating.setComment(inputRating.getComment());
        return ratingRepository.save(rating);
    }

    /**
     * Adds {@link Publisher} to {@link GameVersion}. The publisher will be added to list only if it not yet is there.
     * New publisher will be created if not already in DB.
     * @param gameVersionPublishers list of Publishers for specific GameVersion
     * @param publisherLink BGG XML link element with Publisher data
     */
    private void addPublisherToGameVersion(List<Publisher> gameVersionPublishers, final GameXML.Item.Link publisherLink) {
        // 1. verify if we do not have this publisher in this version already
        if (!gameVersionPublishers.stream()
                .anyMatch(publisher -> publisherLink.getId().equals(publisher.getId()))) {

            // 2. try to find the publisher in DB
            Publisher publisher = publisherRepository.findOne(publisherLink.getId());

            // 3. if not found create one and save in DB
            if (publisher == null) {
                publisher = new Publisher();
                publisher.setId(publisherLink.getId());
                publisher.setName(publisherLink.getValue());
                publisher = publisherRepository.save(publisher);
            }

            // 4. add new publisher to the list
            gameVersionPublishers.add(publisher);

            //publisherRepository.flush();
        }
    }
}

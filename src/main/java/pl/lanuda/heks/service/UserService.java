package pl.lanuda.heks.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.lanuda.heks.dto.UserRegistrationDTO;
import pl.lanuda.heks.model.User;
import pl.lanuda.heks.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findOne(username);
        if (user == null) {
            throw new UsernameNotFoundException("User " + username + " not found");
        }
        return user;
    }

    public User register(UserRegistrationDTO userRegistrationData) {
        User user = new User();
        user.setUsername(userRegistrationData.getUsername());
        user.setPassword(passwordEncoder.encode(userRegistrationData.getPassword()));
        user.setEnabled(true);
        return userRepository.save(user);
    }
}
